import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.callTestCase(findTestCase('Login/Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Belanja/btnBelanja'), 3)

Mobile.tap(findTestObject('Belanja/btnBelanja'), 0)

Mobile.waitForElementPresent(findTestObject('Belanja/btnKatalog'), 3)

Mobile.tap(findTestObject('Belanja/btnKatalog'), 0)

PesanSekarang = Mobile.getText(findTestObject('Belanja/Pesan Sekarang 1'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyMatch(PesanSekarang, 'Pesan Sekarang', false)

PesanSekarang = Mobile.getText(findTestObject('Belanja/Pesan Sekarang 2'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyMatch(PesanSekarang, 'Pesan Sekarang', false)

PesanSekarang = Mobile.getText(findTestObject('Belanja/Pesan Sekarang 3'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyMatch(PesanSekarang, 'Pesan Sekarang', false)

PesanSekarang = Mobile.getText(findTestObject('Belanja/Pesan Sekarang 4'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyMatch(PesanSekarang, 'Pesan Sekarang', false)

Mobile.callTestCase(findTestCase('Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

