import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

//Memulai Aplikasi TD
Mobile.startApplication(apk, false)

Mobile.waitForElementPresent(findTestObject('Login Facebook/LoginFacebook'), 5)

Mobile.tap(findTestObject('Login Facebook/LoginFacebook'), 0)

Mobile.waitForElementPresent(findTestObject('Login Facebook/AddEmailFB'), 5)

Mobile.tap(findTestObject('Login Facebook/AddEmailFB'), 0)

Mobile.setText(findTestObject('Login Facebook/AddEmailFB'), 'yoannafransisca9912@gmail.com', 0)

Mobile.hideKeyboard(FailureHandling.OPTIONAL)

Mobile.waitForElementPresent(findTestObject('Login Facebook/AddPassFacebook'), 5, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Login Facebook/AddPassFacebook'), 0)

Mobile.setText(findTestObject('Login Facebook/AddPassFacebook'), 'yoanna123', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Login Facebook/btnLoginFB'), 0)

