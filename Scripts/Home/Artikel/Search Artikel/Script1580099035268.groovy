import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

Mobile.waitForElementPresent(findTestObject('Artikel/btnArtikel'), 3)

Mobile.tap(findTestObject('Artikel/btnArtikel'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/SearchBox'), 3)

Mobile.tap(findTestObject('Artikel/SearchBox'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/Search'), 3)

Mobile.setText(findTestObject('Artikel/Search'), 'Darah Tinggi', 0)

Mobile.hideKeyboard()

Mobile.waitForElementPresent(findTestObject('Artikel/article'), 3)

Mobile.doubleTap(findTestObject('Artikel/article'), 0)

not_run: Mobile.tap(findTestObject('Artikel/article'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/btnx'), 3)

Mobile.swipe(189, 1885, 125, 728)

Mobile.swipe(307, 1842, 260, 500)

Mobile.tap(findTestObject('Artikel/btnx'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/article'), 3)

Mobile.pressBack()

Mobile.closeApplication()

