import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

Mobile.waitForElementPresent(findTestObject('Artikel/btnArtikel'), 3)

Mobile.tap(findTestObject('Artikel/btnArtikel'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/artikel'), 3)

Mobile.tap(findTestObject('Artikel/artikel'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/btn X'), 3)

Mobile.swipe(110, 1576, 107, 649)

Mobile.swipe(204, 1773, 190, 543)

Mobile.swipe(213, 1803, 191, 592)

Mobile.swipe(234, 1795, 161, 221)

Mobile.tap(findTestObject('Artikel/btn X'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/Bookmark'), 3)

Mobile.tap(findTestObject('Artikel/Bookmark'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/Alert Bookmark'), 3)

Mobile.checkElement(findTestObject('Artikel/Alert Bookmark'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Artikel/btn OK'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/Bookmark'), 3)

Mobile.tap(findTestObject('Artikel/Bookmark'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/AlertUnbookmark'), 3)

Mobile.checkElement(findTestObject('Artikel/AlertUnbookmark'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Artikel/btn OK'), 0)

Mobile.tap(findTestObject('Artikel/Share'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/shareapp'), 3)

Mobile.tap(findTestObject('Artikel/btnCancel'), 0)

Mobile.closeApplication()

