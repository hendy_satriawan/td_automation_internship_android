import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

Mobile.waitForElementPresent(findTestObject('Home-Forum/forum'), 5)

//Tap button forum 
Mobile.tap(findTestObject('Home-Forum/forum'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/btnBookmarkForum'), 3)

Mobile.tap(findTestObject('Bookmark/btnBookmarkForum'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/AlertBookmarkforum'), 3)

Mobile.checkElement(findTestObject('Bookmark/AlertBookmarkforum'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Bookmark/btnOK-Forum'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/btnArtikel'), 3)

Mobile.tap(findTestObject('Artikel/btnArtikel'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/Bookmark'), 3)

Mobile.tap(findTestObject('Artikel/Bookmark'), 0)

Mobile.waitForElementPresent(findTestObject('Artikel/Alert Bookmark'), 3)

Mobile.checkElement(findTestObject('Artikel/Alert Bookmark'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Artikel/btn OK'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/btnBookmark'), 3)

Mobile.tap(findTestObject('Bookmark/btnBookmark'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/ThreadArtikel'), 3)

Mobile.tap(findTestObject('Bookmark/ThreadArtikel'), 0)

Mobile.tap(findTestObject('Bookmark/BtnXArtikel'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/ThreadForum'), 3)

Mobile.tap(findTestObject('Bookmark/ThreadForum'), 0)

Mobile.tap(findTestObject('Bookmark/BtnXForum'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/unbookmark_artikel'), 3)

Mobile.tap(findTestObject('Bookmark/unbookmark_artikel'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/AlertUnbook_Artikel'), 3)

Mobile.checkElement(findTestObject('Bookmark/AlertUnbook_Artikel'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Bookmark/btnOK-Forum'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/unbookmarkForum'), 3)

Mobile.tap(findTestObject('Bookmark/unbookmarkForum'), 0)

Mobile.waitForElementPresent(findTestObject('Bookmark/AlertBookmarkforum'), 3)

Mobile.checkElement(findTestObject('Bookmark/AlertBookmarkforum'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Bookmark/btnOK-Forum'), 0)

Mobile.closeApplication()

