import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Login ke dalam TD
def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

//Menunggu masuk ke dalam aplikasi TD
Mobile.waitForElementPresent(findTestObject('Home-Forum/forum'), 3)

//Tap button forum 
Mobile.tap(findTestObject('Home-Forum/forum'), 0)

//Menunggu masuk ke halaman forum 
Mobile.waitForElementPresent(findTestObject('Home-Forum/cari'), 5)

//Tap search box 
Mobile.tap(findTestObject('Home-Forum/cari'), 0)

//Menginput Thread yang akan dicari
Mobile.setText(findTestObject('Home-Forum/SearchBox'), 'Gula Darah', 0)

//Menyembunyikan keyboard 
Mobile.hideKeyboard()

Mobile.scrollToText('Tandai', FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.swipe(894, 1902, 864, 1718)

//Tap Btn Bookmark
Mobile.doubleTap(findTestObject('Home-Forum/Tandai'), 0)

//menunggu alert Success Bookmark
Mobile.waitForElementPresent(findTestObject('Home-Forum/success bookmark'), 3)

//Muncul Alert Success bookmark
Mobile.getElementTopPosition(findTestObject('Home-Forum/success bookmark'), 0, FailureHandling.STOP_ON_FAILURE)

//Tap Ok untuk menyetujui bookmark
Mobile.tap(findTestObject('Home-Forum/btnOK'), 0)

//Menunggu kembali ke thread untuk unbookmark
Mobile.waitForElementPresent(findTestObject('Home-Forum/Tandai'), 3)

//Tap btn bookmark untuk melakukan unbookmark 
Mobile.tap(findTestObject('Home-Forum/Tandai'), 0)

//Mobile.tap(findTestObject('Home-Forum/android.view.ViewGroup17'), 0)
//menunggu alert unbookmark 
Mobile.waitForElementPresent(findTestObject('Home-Forum/success bookmark'), 3)

//muncul alert unbookmark
Mobile.verifyElementExist(findTestObject('Home-Forum/success bookmark'), 0, FailureHandling.OPTIONAL)

//Tap OK untuk menyetujui unbookmark
Mobile.tap(findTestObject('Home-Forum/btnOK'), 0)

//Menunggu untuk kembali ke Thread untuk melakukan share
Mobile.waitForElementPresent(findTestObject('Home-Forum/share (1)'), 3)

//Tap btn share 
Mobile.tap(findTestObject('Home-Forum/share (1)'), 0)

//Mobile.tap(findTestObject('Home-Forum/share'), 0)
//menunggu element share aplikasi TD kepada org lain
Mobile.waitForElementPresent(findTestObject('Home-Forum/btnCancel'), 3)

//tap btn cancel untuk membatalkan share TD
Mobile.tap(findTestObject('Home-Forum/btnCancel'), 0)

//Menunggu untuk kembali ke thread
Mobile.waitForElementPresent(findTestObject('Home-Forum/ThreadAlatPengukur'), 3)

//Tap thread untuk melihat detail thread
Mobile.tap(findTestObject('Home-Forum/ThreadAlatPengukur'), 0)

//menunggu btn X untuk kembali ke halaman Thread
Mobile.waitForElementPresent(findTestObject('Home-Forum/btnX2'), 3)

//Tap btn X untuk kembali ke halaman thread
Mobile.tap(findTestObject('Home-Forum/btnX2'), 0)

//menunggu untuk ke halaman thread
Mobile.waitForElementPresent(findTestObject('Home-Forum/BtnX halaman forum'), 3)

//Tap btn X untuk kembali ke halaman forum
Mobile.tap(findTestObject('Home-Forum/BtnX halaman forum'), 0)

//Selesai 
Mobile.closeApplication()

