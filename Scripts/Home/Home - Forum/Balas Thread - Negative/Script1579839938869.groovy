import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

//Menunggu masuk ke dalam aplikasi TD
Mobile.waitForElementPresent(findTestObject('Home-Forum/forum'), 5)

//Tap button forum 
Mobile.tap(findTestObject('Home-Forum/forum'), 0)

//Menunggu masuk ke halaman forum 
Mobile.waitForElementPresent(findTestObject('Home-Forum/cari'), 5)

//Tap search box 
Mobile.tap(findTestObject('Home-Forum/cari'), 0)

//Menginput Thread yang akan dicari
Mobile.setText(findTestObject('Home-Forum/SearchBox'), 'Kolestrol', 0)

//Menyembunyikan keyboard 
Mobile.hideKeyboard()

//Menunggu Thread tentang kolestrol muncul 
Mobile.waitForElementPresent(findTestObject('Home-Forum/ThreadKolestrol'), 3)

//Tap Thread Kolestrol
Mobile.tap(findTestObject('Home-Forum/ThreadKolestrol'), 0)

Mobile.tap(findTestObject('Home-Forum/ThreadKolestrol'), 0)

//Menunggu membuka detail thread kolestrol
Mobile.waitForElementPresent(findTestObject('Home-Forum/Balas Thread'), 3)

//Tap button Balas untuk membalas Thread 
Mobile.tap(findTestObject('Home-Forum/Balas Thread'), 0)

//Menunggu page balas Thread terbuka 
Mobile.waitForElementPresent(findTestObject('Home-Forum/Add Komen'), 3)

//Tap untuk mengeluarkan keyboard dan menambahkan teks komentar 
Mobile.tap(findTestObject('Home-Forum/Add Komen'), 0)

//Memasukkan teks komentar Thread 
Mobile.setText(findTestObject('Home-Forum/Add Komen'), '', 0)

//Menyembunyikan keyboard setelah selesai memasukkan komentar 
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Home-Forum/BtnKirim'), 0)

Mobile.waitForElementPresent(findTestObject('Home-Forum/AlertKomentar'), 3)

Mobile.verifyElementExist(findTestObject('Home-Forum/AlertKomentar'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Home-Forum/btnOK'), 0)

Mobile.waitForElementPresent(findTestObject('Home-Forum/BtnXKomen'), 3)

//Tap button X untuk keluar dari page balas Thread
Mobile.tap(findTestObject('Home-Forum/BtnXKomen'), 0)

//Menunggu kembali ke page detail thread
Mobile.waitForElementPresent(findTestObject('Home-Forum/Balas komen'), 3)

//Tap button Balas dalam kolom komentar 
Mobile.tap(findTestObject('Home-Forum/Balas komen'), 0)

//Menunggu page komentar muncul
Mobile.waitForElementPresent(findTestObject('Home-Forum/tmbhKomentar'), 3)

//Tap page komentar untuk memasukkan teks komentar 
Mobile.tap(findTestObject('Home-Forum/tmbhKomentar'), 0)

//Memasukkan teks komentar 
Mobile.setText(findTestObject('Home-Forum/tmbhKomentar'), '', 0)

//Menyembunyikan keyboard setelah memasukkan teks komentar 
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Home-Forum/BtnKirimKomentar'), 0)

Mobile.verifyElementExist(findTestObject('Home-Forum/AlertKomentar'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Home-Forum/btnOkKomentar'), 0)

Mobile.tap(findTestObject('Home-Forum/BtnXKomen'), 0)

//Menunggu kembali ke halaman detail thread 
Mobile.waitForElementPresent(findTestObject('Home-Forum/klmKomentar'), 3)

//Tap kolom komentar 
Mobile.tap(findTestObject('Home-Forum/klmKomentar'), 0)

//Scroll Down thread komentar
Mobile.swipe(782, 1603, 774, 720)

//Menunggu komentar box 
Mobile.waitForElementPresent(findTestObject('Home-Forum/Komentari'), 3)

//Tap komentar box untuk memasukkan teks komentar 
Mobile.tap(findTestObject('Home-Forum/Komentari'), 0)

//Memasukkan teks komentar 
Mobile.setText(findTestObject('Home-Forum/Komentari'), '', 0)

//Menyembunyikan keyboard setelah memasukkan komentar 
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Home-Forum/BtnBalas'), 0)

Mobile.tap(findTestObject('Home-Forum/BtnBalas'), 0)

Mobile.waitForElementPresent(findTestObject('Home-Forum/AlertKomentar'), 3)

Mobile.verifyElementExist(findTestObject('Home-Forum/AlertKomentar'), 0, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Home-Forum/btnOkKomentar'), 0)

Mobile.tap(findTestObject('Home-Forum/BtnBack'), 0)

//Menunggu button X unutk kembali ke page thread
Mobile.waitForElementPresent(findTestObject('Home-Forum/BtnXKomen'), 3)

//Tap button X untuk kembali ke page thread
Mobile.tap(findTestObject('Home-Forum/BtnXKomen'), 0)

//menunggu membuka page thread
Mobile.waitForElementPresent(findTestObject('Home-Forum/BtnX halaman forum'), 3)

//Tap button X untuk kembali ke page forum 
Mobile.tap(findTestObject('Home-Forum/BtnX halaman forum'), 0)

//Selesai
Mobile.closeApplication()

