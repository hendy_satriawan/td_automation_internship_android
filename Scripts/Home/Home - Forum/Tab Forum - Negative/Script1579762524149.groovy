import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

//Menunggu masuk ke dalam aplikasi TD
Mobile.waitForElementPresent(findTestObject('Home-Forum/forum'), 3)

//Tap button forum 
Mobile.tap(findTestObject('Home-Forum/forum'), 0)

//Menunggu masuk ke halaman forum 
Mobile.waitForElementPresent(findTestObject('Home-Forum/cari'), 3)

//Tap search box 
Mobile.tap(findTestObject('Home-Forum/cari'), 0)

//Menginput Thread yang akan dicari
Mobile.setText(findTestObject('Home-Forum/SearchBox'), '!@##$%%', 0)

Mobile.hideKeyboard()

Mobile.verifyElementText(findTestObject('Home-Forum/AlertSearch'), 'Opppss.. Pencarian Anda Tidak Ditemukan', FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Home-Forum/BtnX halaman forum'), 0)

Mobile.closeApplication()

