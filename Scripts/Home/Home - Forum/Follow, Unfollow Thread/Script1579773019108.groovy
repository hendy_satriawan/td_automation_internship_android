import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

//Menunggu masuk ke dalam aplikasi TD
Mobile.waitForElementPresent(findTestObject('Home-Forum/forum'), 5)

//Tap button forum 
Mobile.tap(findTestObject('Home-Forum/forum'), 0)

Mobile.waitForElementPresent(findTestObject('Home-Forum/Thread'), 3)

Mobile.tap(findTestObject('Home-Forum/Thread'), 0)

Mobile.waitForElementPresent(findTestObject('Home-Forum/Follow'), 3)

Mobile.tap(findTestObject('Home-Forum/Follow'), 0)

Mobile.waitForElementPresent(findTestObject('Home-Forum/unfollow'), 3)

Mobile.tap(findTestObject('Home-Forum/unfollow'), 0)

Mobile.tap(findTestObject('Home-Forum/btnX'), 0)

Mobile.closeApplication()

