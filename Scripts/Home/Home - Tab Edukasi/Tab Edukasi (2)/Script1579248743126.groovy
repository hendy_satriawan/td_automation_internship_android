import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

//Menunggu Membuka halaman Home
Mobile.waitForElementPresent(findTestObject('Home - Tab Edukasi/tab Edukasi'), 3)

//Tap button Edukasi
Mobile.tap(findTestObject('Home - Tab Edukasi/tab Edukasi'), 0)

Mobile.swipe(861, 1756, 903, 631)

Mobile.swipe(827, 1692, 805, 740)

Mobile.waitForElementPresent(findTestObject('Home - Tab Edukasi (2)/MengurangiRisikoKomp'), 3)

Mobile.swipe(886, 1166, 102, 1124)

Mobile.swipe(875, 1206, 117, 1159)

Mobile.doubleTap(findTestObject('Home - Tab Edukasi (2)/MengurangiRisikoKomp'), 0)

Mobile.swipe(898, 1900, 981, 300)

Mobile.tap(findTestObject('Home - Tab Edukasi (2)/btnBack'), 0)

Mobile.closeApplication()

