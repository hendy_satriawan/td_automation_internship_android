import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Mobile.startApplication(apk, false)

Mobile.waitForElementPresent(findTestObject('Home-Forum/forum'), 5)

Mobile.tap(findTestObject('Home-Forum/forum'), 0)

Mobile.waitForElementPresent(findTestObject('thread/btnCreate'), 5)

Mobile.tap(findTestObject('thread/btnCreate'), 0)

Mobile.waitForElementPresent(findTestObject('thread/JdlThread'), 5)

Mobile.setText(findTestObject('thread/JdlThread'), '', 0)

Mobile.waitForElementPresent(findTestObject('thread/Deskripsi thread'), 5)

Mobile.tap(findTestObject('thread/Deskripsi thread'), 0)

Mobile.setText(findTestObject('thread/Deskripsi thread'), '', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('thread/Kirim'), 0)

Mobile.verifyElementExist(findTestObject('thread/Alert Judul'), 3, FailureHandling.OPTIONAL)

Mobile.verifyElementExist(findTestObject('thread/Alert Deksripsi'), 3, FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('thread/tipe'), 0)

Mobile.tap(findTestObject('thread/tipe Sharing'), 0)

Mobile.tap(findTestObject('thread/Kategori'), 0)

Mobile.tap(findTestObject('thread/Kategori Hidup sehat'), 0)

Mobile.tap(findTestObject('thread/BtnX'), 0)

Mobile.closeApplication()

