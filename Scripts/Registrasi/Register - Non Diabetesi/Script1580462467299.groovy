import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.github.javafaker.Faker as Faker

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

Faker faker = new Faker()

String name = faker.name().fullName()

String firstName = faker.name().firstName()

String email = firstName + '@yopmail.com'

println(firstName)

//Memulai Aplikasi TD
Mobile.startApplication(apk, false)

Mobile.tap(findTestObject('Registrasi/addNama'), 0)

Mobile.setText(findTestObject('Registrasi/addNama'), firstName, 0)

Mobile.tap(findTestObject('Registrasi/AddEmail'), 0)

Mobile.setText(findTestObject('Registrasi/AddEmail'), email, 0)

Mobile.tap(findTestObject('Registrasi/AddPass'), 0)

Mobile.setText(findTestObject('Registrasi/AddPass'), 'Abc123', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Registrasi/btnDaftar'), 0)

Mobile.waitForElementPresent(findTestObject('Registrasi/addTglLahir'), 0)

Mobile.tap(findTestObject('Registrasi/addTglLahir'), 0)

'swipe bulan kelahiran'
Mobile.swipe(192, 1039, 258, 1821)

'Swipe Tanggal Kelahiran'
Mobile.swipe(482, 1083, 526, 1832)

'Swipe Tahun Kelahiran'
Mobile.swipe(821, 1024, 856, 1939)

Mobile.tap(findTestObject('Registrasi/btnOkTgl'), 0)

Mobile.tap(findTestObject('Registrasi/AddTlp'), 0)

Mobile.setText(findTestObject('Registrasi/AddTlp'), '087665334567', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Registrasi/JnsKelamin'), 0)

Mobile.tap(findTestObject('Registrasi/Laki2'), 0)

Mobile.tap(findTestObject('Registrasi/TipeUser'), 0)

Mobile.tap(findTestObject('Registrasi/NonDiabetesi'), 0)

Mobile.tap(findTestObject('Registrasi/AddAlamat'), 0)

Mobile.setText(findTestObject('Registrasi/AddAlamat'), 'Jl.Sukasuka', 0)

Mobile.hideKeyboard()

Mobile.doubleTap(findTestObject('Registrasi/btnSimpan'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/btnProfile'), 3, FailureHandling.STOP_ON_FAILURE)

//Melihat profile sebagai user diabetesi
Mobile.tap(findTestObject('Profile/btnProfile'), 0)

Mobile.swipe(904, 1833, 849, 542)

Mobile.swipe(861, 1897, 857, 776)

Mobile.tap(findTestObject('Logout/btnLogout (1)'), 0)

