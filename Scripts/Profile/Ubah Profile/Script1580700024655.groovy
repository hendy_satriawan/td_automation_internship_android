import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def apk = PathUtil.relativeToAbsolutePath(GlobalVariable.appName, RunConfiguration.getProjectDir())

//Memulai Aplikasi TD
Mobile.startApplication(apk, false)

Mobile.waitForElementPresent(findTestObject('profile1/btnProfile'), 3)

Mobile.tap(findTestObject('profile1/btnProfile'), 0)

Mobile.waitForElementPresent(findTestObject('profile1/btnUbahProfile'), 3)

Mobile.tap(findTestObject('profile1/btnUbahProfile'), 0)

Mobile.waitForElementPresent(findTestObject('profile1/addtgl'), 3)

Mobile.tap(findTestObject('profile1/addtgl'), 0)

Mobile.swipe(208, 1086, 217, 1887)

Mobile.swipe(458, 1053, 388, 1987)

Mobile.swipe(696, 1072, 611, 1987)

Mobile.tap(findTestObject('profile1/btnoktgl'), 0)

Mobile.tap(findTestObject('profile1/addtlp'), 0)

Mobile.setText(findTestObject('profile1/addtlp'), '0876865436', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('profile1/jnskelamin'), 0)

Mobile.tap(findTestObject('profile1/perempuan'), 0)

Mobile.tap(findTestObject('profile1/tipediabetesi'), 0)

Mobile.tap(findTestObject('profile1/diabetesi'), 0)

Mobile.tap(findTestObject('profile1/jnsdiabetesi'), 0)

Mobile.tap(findTestObject('profile1/diabetesitype1'), 0)

Mobile.tap(findTestObject('profile1/addalamat'), 0)

Mobile.setText(findTestObject('profile1/addalamat'), 'Jl. Indah', 0)

Mobile.hideKeyboard()

Mobile.swipe(957, 1986, 931, 1622)

Mobile.doubleTap(findTestObject('profile1/btnsimpan'), 0)

