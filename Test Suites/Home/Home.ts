<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Home</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>91056e39-0ee7-4306-9cc4-e76a7716c550</testSuiteGuid>
   <testCaseLink>
      <guid>e03cba94-b480-46f8-8363-0fbd58743d66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Home - Tab Edukasi/Tab Edukasi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b37b0336-e85a-43c0-a44a-5f17b78dc5a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Home - Tab Edukasi/Tab Edukasi (2)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2131b55-a6da-4200-a6aa-a1391905e76d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Home - Forum/Search,Bookmark,Share - positive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>012be1b8-07d9-4e75-9c0c-c6d899c41fb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Home - Forum/Tab Forum - Negative</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>566ab7a5-fec8-4d6f-a436-6c633d527bd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Home - Forum/Follow, Unfollow Thread</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c99afda-908b-4cac-bdd4-c69e604ba6ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Artikel/Buka Artikel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5bb847f-c137-470b-9cfc-7d30925b5a88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Thread/Create Thread - Positive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca56a8d4-6cc2-49b4-b196-e8ac4a648460</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Home - Forum/Balas Thread - Positive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e66f713-9d8e-4a68-a0f5-094852138941</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Home - Forum/Balas Thread - Negative</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92a98c7e-fb50-4ad3-9e8a-e5067c36e502</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Thread/Create Thread - Negative</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d69f03b3-b3f6-491f-99e8-38d74f0fb827</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Artikel/Search Artikel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>216f5198-fdc7-456a-8955-232a5d677b32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/Bookmark/Bookmark</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
